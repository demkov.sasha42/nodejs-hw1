// requires...
const fs = require('fs');
const fsp = require('fs/promises');
const express = require('express');
const path = require("path");
const app = express();
const port = 8080;
const filesFolder = path.resolve('./files');
const extensions = ['txt', 'json', 'xml', 'js', 'log', 'yaml'];
// constants...

const createFile = (req, res, next) => {
  try {
    const { filename, content } = req.body;

    if (filename === undefined) {
      res.status(400).json({message: 'Please specify "filename" parameter'});
      return;
    }
    if (content === undefined) {
      res.status(400).json({message: 'Please specify "content" parameter'});
      return;
    }
    const fileExtension = path.extname(filename).split('.')[1];

    if (extensions.includes(fileExtension)) {
      fs.writeFileSync(path.join(filesFolder, filename), content, { flag: 'a+' });
      res.status(200).send({ "message": "File created successfully" });
    } else {
      res.status(400).json({
        message: `File extension should be one of the followings: ${extensions}`
      });
    }

  } catch (error) {
    console.error(error.message);
    res.status(500).json({message: 'Server error'});
  }
}

const getFiles = (req, res, next) => {
  const filesList = [];
  try {
    fs.readdir(filesFolder, (err, result) => {
      if (err) {
        res.status(400).json({message: 'Client error'});
      }
      result.forEach(file => {
        filesList.push(file);
      })
      res.status(200).send({
        "message": "Success",
        "files": filesList});
    })
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
}

const getFile = async (req, res, next) => {
  try {
    let uploadedDate;
    let extension = path.extname(req.url).split('.')[1];
    fs.stat(filesFolder + req.url, (error, stats) => {
      if (error) {
        console.log(error);
        return;
      }
      uploadedDate = stats.birthtime;
    });
    res.status(200).send({
      "message": "Success",
      "filename": path.basename(req.url),
      "content": await fsp.readFile(filesFolder + req.url, { encoding: 'utf8' }),
      "extension": extension,
      "uploadedDate": uploadedDate});
  } catch (error) {
    res.status(400).json({message: 'Server error'});
  }
}

// Other functions - editFile, deleteFile

const editFile = (req, res, next) => {
  try {
    const {content} = req.body;
    if (content === undefined) {
      res.status(400).json({message: 'Client error'});
    } else {
      fs.writeFileSync(filesFolder + req.url, content, { flag: 'w' });
      res.status(200).send({message: 'File was edited'});
    }
  } catch (error) {
    res.status(500).send({message: 'Server error'});
  }
}

const deleteFile = (req, res, next) => {
  try {
    if (fs.existsSync(filesFolder + req.url)) {
      fs.unlink(filesFolder + req.url, (err) => {
        if (err) {
          res.status(400).json({message: 'Something wrong'});
        }
        res.status(200).json({message: 'File was deleted'});
      })
    } else {
      res.status(400).json({message: `File doesn't exist`});
    }
  } catch (error) {
    res.status(500).json({message: 'Server error'});
  }
}

// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}